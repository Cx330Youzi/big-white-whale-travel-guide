

SET FOREIGN_KEY_CHECKS=0;

-- ----------------用户表------------
DROP TABLE IF EXISTS `user_accounts`;
CREATE TABLE `user_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `cover` varchar(255) NOT NULL default '' COMMENT '帐户页封面',
  `openId` varchar(255) NOT NULL default '' COMMENT '用户唯一标识',
  `nickName` varchar(255) NOT NULL default '' COMMENT '用户名',
  `language` varchar(64) NOT NULL default '' COMMENT '语言',
  `city` varchar(64) NOT NULL default '' COMMENT '市',
  `province` varchar(64) NOT NULL default '' COMMENT '省',
  `country` varchar(64) NOT NULL default '' COMMENT '国家',
  `avatarUrl` varchar(1024) NOT NULL default '' COMMENT '头像',
  `gender` tinyint(3) unsigned NOT NULL default '0' COMMENT '性别',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  unique key `idx_openid` (`openId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into user_accounts(cover,openId,nickName,`language`,city,province,country,avatarUrl,
gender)values ("http://image.ithlw.xyz/20181221/104828946c882e.jpg",'omKcA5djYVIY4rkayOjmoK_dU9ZE','Layne',
'zh_CN','Pudong New District','ShangHai','China','https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLD8eiaUmia6LK73gfyZwYO1MAFqrxwbrN3L3q62pD850iauiawUF6icqL7loZBtctbyyXf1Dxz7AzJicxw/132',1);



-- ----------------用户表------------
DROP TABLE IF EXISTS `user_token`;
CREATE TABLE `user_token` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `userId` bigint(20) unsigned NOT NULL default '0' COMMENT '用户 id',
  `openId` varchar(255) NOT NULL default '' COMMENT '用户唯一标识',
  `token` varchar(255) NOT NULL default '' COMMENT 'token',
  `update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  unique key `idx_openid` (`openId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ------------关注----------------
DROP TABLE IF EXISTS `guanzhu`;
CREATE TABLE `guanzhu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `user_id_1` bigint(20) unsigned NOT NULL default '0' COMMENT '谁关注的',
  `user_id_2` bigint(20) unsigned NOT NULL default '0' COMMENT '被关注的',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  PRIMARY KEY (`id`),
  UNIQUE key `idx_user12` (`user_id_1`,`user_id_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ------------收藏/喜欢----------------
DROP TABLE IF EXISTS `shoucang`;
CREATE TABLE `shoucang` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `user_id` bigint(20) unsigned NOT NULL default '0' COMMENT '用户id',
  `waypoint_id` bigint(20) unsigned NOT NULL default '0' COMMENT '足迹id',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  PRIMARY KEY (`id`),
  UNIQUE key `idx_user_trip` (`user_id`,`waypoint_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into shoucang(user_id,waypoint_id)values (1,1);


-- ------------评论----------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `user_id` bigint(20) unsigned NOT NULL default '0' COMMENT '用户id',
  `waypoint_id` bigint(20) unsigned NOT NULL default '0' COMMENT '足迹id',
  `comments` varchar(1024) NOT NULL default '' COMMENT '评论',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into comments(user_id,waypoint_id,`comments`)values (1,1,'支持你，你是最棒的，爱你，Layne');

-- ------------旅行----------------
DROP TABLE IF EXISTS `trip`;
CREATE TABLE `trip` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `user_id` bigint(20) unsigned NOT NULL default '0' COMMENT '用户id',
  `view_count` bigint(20) unsigned NOT NULL default '0' COMMENT '用户id',
  `title` varchar(256) NOT NULL default '' COMMENT '旅行名',
  `start_date` bigint(20) unsigned NOT NULL default '0' COMMENT '开始时间' ,
  `end_date` bigint(20) unsigned NOT NULL default '0' COMMENT '结束时间' ,
  `cover_image_w640` varchar(1024) NOT NULL default '' COMMENT '旅行封面',
  `popular_place_str` varchar(64) NOT NULL default '' COMMENT '旅行地点',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into trip(user_id,view_count,title,start_date,end_date,cover_image_w640,popular_place_str)
values (1,111,'清明上海行',1554048000000,1554652800000,'http://image.ithlw.xyz/20181221/141907865812ea.jpg','中国.上海');


-- ------------足迹----------------
DROP TABLE IF EXISTS `waypoint`;
CREATE TABLE `waypoint` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长 id',
  `trip_id` bigint(20) unsigned NOT NULL default '0' COMMENT 'tripid',
  `trip_date` bigint(20) unsigned NOT NULL default '0' COMMENT '天id',
  `photo_info_h` int(11) unsigned NOT NULL default '0' COMMENT '高',
  `photo_info_w` int(11) unsigned NOT NULL default '0' COMMENT '图宽',
  `text` varchar(1024) NOT NULL default '' COMMENT '文本',
  `photo_webtrip` varchar(1024) NOT NULL default '' COMMENT '足迹照片链接',
  `create_time` bigint(20) unsigned NOT NULL default '0' COMMENT '创建时间' ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into waypoint(trip_id,trip_date,photo_info_h,photo_info_w,text,photo_webtrip)
values (1,1554048000000,600,800,'开心','http://image.ithlw.xyz/20181221/104828946c882e.jpg');


insert into waypoint(trip_id,trip_date,photo_info_h,photo_info_w,text,photo_webtrip)
values (1,1554220800000,600,800,'开心','http://image.ithlw.xyz/20181221/104828946c882e.jpg');

